% -----------------------------------------------------------------------------------------------------------
% 
% This LaTeX class file is intended to help authors prepare manuscripts for submission to EARTHQUAKE SPECTRA.
% 
% You are NOT permitted to modify this file.
% 
% Commercial use of this file in any form is PROHIBITED.
% 
% This file is provided 'as is', WITHOUT ANY WARRANTY; without even the implied warranty of merchantability
% or fitness for a particular purpose.
% 
% Created by
%    Reagan Chandramohan
%    Department of Civil and Environmental Engineering
%    Stanford University
% 
% for
%    Earthquake Spectra
% 
% Version 1.0
% 19-Aug-2015
% 
% -----------------------------------------------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{spectra}[2015/08/19 Earthquake Spectra]

\LoadClass[12pt,letterpaper]{article}
\RequirePackage{mathptmx,graphicx,amsmath,changepage,titlesec,caption,fix-cm,microtype}
\RequirePackage[hyphens]{url}
\RequirePackage[colorlinks,allcolors=blue]{hyperref}
\RequirePackage[top=0.75in,bottom=1.00in,left=1.10in,right=1.10in]{geometry}

% Define font sizes and spacing
\newcommand{\titlesize}{\fontsize{22}{26pt}\selectfont}
\newcommand{\authorsize}{\fontsize{14}{18pt}\selectfont}
\newcommand{\footnotemarksize}{\fontsize{8}{18pt}\selectfont}
\newcommand{\meerisize}{\fontsize{11}{18pt}\selectfont}
\renewcommand{\normalsize}{\fontsize{12}{20pt}\selectfont}
\newcommand{\subsectionsize}{\fontsize{11}{20pt}\selectfont}
\newcommand{\refsize}{\fontsize{11}{14pt}\selectfont}
\newcommand{\tablesize}{\fontsize{11}{16pt}\selectfont}

% Define title and author formatting
\newcommand{\meeri}{{\meerisize\textbf{M.EERI}}}
\newcommand{\affilnum}[1]{\textsuperscript{\footnotesize#1)}}
\newcommand{\affiladdr}[2]{\footnotetext[#1]{#2}}
\renewcommand{\thefootnote}{\footnotemarksize\alph{footnote})}
\renewcommand{\@maketitle}{
    \renewcommand{\thefootnote}{\footnotemarksize\textbf{\alph{footnote})}}
    \begin{flushleft}
        \titlesize\textbf{\@title}\par
    \end{flushleft}
    \vspace{4pt}
    \authorsize\textbf{\@author}\par
    \vspace{10pt}
}

% Define abstract formatting
\renewenvironment{abstract}{\begin{adjustwidth}{0.38in}{0.38in}}{\end{adjustwidth}}

% Define section title formatting
\titlelabel{}
\titleformat*{\section}{\centering\bfseries\uppercase}
\titlespacing*{\section}{0pt}{12pt}{6pt}
\titleformat*{\subsection}{\subsectionsize\bfseries\uppercase}
\titlespacing*{\subsection}{0pt}{9pt}{6pt}
\titleformat*{\subsubsection}{\bfseries}
\titlespacing*{\subsubsection}{0pt}{6pt}{3pt}
\titleformat{\paragraph}[hang]{\itshape}{}{}{}
\titlespacing*{\paragraph}{0pt}{3pt}{0pt}

% Define figure and table caption formatting
\DeclareCaptionFormat{capformat}{\fontsize{11}{14pt}\selectfont#1#2#3}
\captionsetup{format=capformat,labelsep=period,justification=justified,labelfont={bf}}

% Define paragraph indent and spacing
\setlength{\parindent}{0.25in}
\setlength{\parskip}{6pt}

% Define line numbers
\RequirePackage{lineno}
%\renewcommand{\linenumberfont}{\normalfont\scriptsize}
\linenumbers

% Prevent line numbers from not showing in paragraphs preceding math environments like 'align'
% Uses recommendations from http://phaseportrait.blogspot.com/2007/08/lineno-and-amsmath-compatibility.html
\newcommand*\patchAmsMathEnvironmentForLineno[1]{%
    \expandafter\let\csname old#1\expandafter\endcsname\csname #1\endcsname%
    \expandafter\let\csname oldend#1\expandafter\endcsname\csname end#1\endcsname%
    \renewenvironment{#1}{\linenomath\csname old#1\endcsname}{\csname oldend#1\endcsname\endlinenomath}%
}
\newcommand*\patchBothAmsMathEnvironmentsForLineno[1]{%
    \patchAmsMathEnvironmentForLineno{#1}%
    \patchAmsMathEnvironmentForLineno{#1*}%
}
\newcommand\linenumfix{%
    \AtBeginDocument{%
        \patchBothAmsMathEnvironmentsForLineno{equation}%
        \patchBothAmsMathEnvironmentsForLineno{align}%
        \patchBothAmsMathEnvironmentsForLineno{flalign}%
        \patchBothAmsMathEnvironmentsForLineno{alignat}%
        \patchBothAmsMathEnvironmentsForLineno{gather}%
        \patchBothAmsMathEnvironmentsForLineno{multline}%
    }%
}

% Define bibliography formatting
\RequirePackage{natbib}
\renewcommand{\refname}{\uppercase{References}}
\renewcommand{\bibfont}{\refsize}
\renewcommand{\bibsep}{3pt}

% Handle special characters
\RequirePackage[utf8]{inputenc}

% Define URL and DOI formatting
\newcommand{\urlprefix}{}
\renewcommand\UrlFont{\rmfamily}
